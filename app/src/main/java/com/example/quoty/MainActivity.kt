package com.example.quoty

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.example.quoty.databinding.ActivityMainBinding

class MainActivity : ComponentActivity() {
    // Binding for later use
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO: Initialize view binding and set view
        // TODO("View binding init")

        // Composable interop magic - take compose view and call composable function on it
        binding.composeView.apply {
            setContent {
                MaterialTheme {
                    // TODO: Call composable function
                }
            }
        }

        // TODO: TIP: Split code into functions = please, don't write spaghetti code.
        // TODO: Implement quote generation using onClick attribute.
        // TODO: Implement quote generation using `findViewById` function.
        // TODO: Implement quote generation using view binding.
        // TODO: Implement quote generation using composable function
    }
}
