package com.example.quoty

val quotes = listOf(
    "You cannot spell \"Compose\" without \"cope\".",
    "Why fall in love when you can fall asleep.",
    "Gradle Project Sync Failed",
    "If it doesn't work, invalidate caches and restart.",
    "This project is surely well tested.",
    "Live. Love. Lifecycle.",
    "I am tired of the whole concept of humans now.",
    "\"Composable\" is quite close to \"Compostable\".",
    "Animations are not that hard. /s",
    "Kotlin Coroutines are not that hard. /s",
)